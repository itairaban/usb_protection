#include <libudev.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <unistd.h>
#include <iostream>
#include <thread>
#include <vector>
#include <fstream>
#include <sstream>
#include <exception>
#include "UsbDevice.h"
#include "Colors.h"


#define SUBSYSTEM_FILTER "usb"
#define TRUE 1
#define FALSE !TRUE
#define MAX_PORT 65535
#define DEFAULT_SERVER_ADDR "127.0.0.1"
#define DEFAULT_SERVER_PORT 31337

static void enumerate_devices(struct udev *udev);
static void print_device_info(struct udev_device *device);
static void process_device(struct udev_device *device);
static void monitor_devices(struct udev *dev);
server_config readConfigFile();

std::vector<std::thread> device_processors;
server_config configFile;

int main(void)
{
    struct udev *udev = udev_new();
    Color::Modifier red(Color::FG_RED);
    Color::Modifier green(Color::FG_GREEN);
    Color::Modifier def(Color::FG_DEFAULT);

    if (geteuid() != 0) // Checks if the program has root
    {
        std::cerr << green << "$ " << red << "sudo give me sudo!!!" << def << std::endl;
        return 1;
    }

    try
    {
        configFile = readConfigFile();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    
    std::cout << "Server addr: " << configFile.addr << std::endl; // Debug
    std::cout << "Server port: " << configFile.port << std::endl << std::endl; // Debug

    if (!udev)
    {
        std::cerr << red << "Failed to create udev" << def << std::endl;
        return 1;
    }

    //enumerate_devices(udev);
    monitor_devices(udev);
    udev_unref(udev);

    return 0;
}

/** 
 * The function monitors the device events.
 * The function uses select() in order to use the monitor in a non-blocking way
 * Input: udev
 * Output: None
*/
static void monitor_devices(struct udev *udev)
{
    int fd, ret;
    struct udev_monitor *monitor = udev_monitor_new_from_netlink(udev, "udev");

    udev_monitor_filter_add_match_subsystem_devtype(monitor, SUBSYSTEM_FILTER, NULL); // Adds filter to the monitor
    udev_monitor_enable_receiving(monitor);
    fd = udev_monitor_get_fd(monitor); // Gets file descriptor for the monitor

    while(TRUE)
    {
        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(fd, &fds);
        
        ret = select(fd+1, &fds, NULL, NULL, NULL); // Calls select with the monitor's file descriptor
        if (ret <= 0)
        {
            fprintf(stderr, "An error occured\n");
            break;
        }

        if(FD_ISSET(fd, &fds))
        {
            struct udev_device *device = udev_monitor_receive_device(monitor);
            process_device(device);
        }

    }
    udev_monitor_unref(monitor);
}


/**
 * The function enumerates the usb devices
 * Input: struct udev *udev
 * Output: None
*/
static void enumerate_devices(struct udev *udev)
{
    struct udev_enumerate *enumerate;
    struct udev_list_entry *devices, *dev_list_entry;
    struct udev_device *dev;


    enumerate = udev_enumerate_new(udev);
    udev_enumerate_add_match_subsystem(enumerate, SUBSYSTEM_FILTER); // Adds usb devices filtering
    udev_enumerate_scan_devices(enumerate); // Scan for devices
    devices = udev_enumerate_get_list_entry(enumerate);
    
    // Process each usb device
    udev_list_entry_foreach(dev_list_entry, devices)
    {
        const char *dev_path;

        dev_path = udev_list_entry_get_name(dev_list_entry); // Gets the file name of the /sys entry of the current device
        dev = udev_device_new_from_syspath(udev, dev_path); // Creates device based on the current device path

        process_device(dev);
    }
    udev_enumerate_unref(enumerate);
}

/** 
 * The function process an udev device
 * Input: ptr to the device
 * Output: None
*/
static void process_device(struct udev_device *device)
{
    if(device)
    {
        if(udev_device_get_devnode(device))
        {
            UsbDevice *usb_device = new UsbDevice(device, configFile);
            device_processors.push_back(std::thread(&UsbDevice::processDevice, usb_device));
            // std::cout << usb_device << std::endl;
        }
        // udev_device_unref(device); // Free the device
    }
}

/**
 * The function reads the configuration file. The fucntion thrpws exceptions in case of invalid file arguments
 * Input: none
 * Output: server_config struct contains the configurations
*/
server_config readConfigFile()
{
    server_config config;
    std::fstream configurationFile;
    configurationFile.open("config", std::ios::in);
    if (configurationFile.is_open())
    {
        std::string first_line;
        std::string server_addr;
        getline(configurationFile, first_line);
        server_addr = first_line.substr(first_line.find("=")+1); // Gets only the server address
        std::string second_line;

        int server_port;
        getline(configurationFile, second_line);
        std::istringstream(second_line.substr(second_line.find("=")+1)) >> server_port;

        if (server_port < 1 || server_port > MAX_PORT)
        {
            throw std::runtime_error("Invalid port");
        } 
        config.addr = server_addr;
        config.port = server_port;
        configurationFile.close();
    }
    else // File is not exists
    {
        std::cout << "[-] Creating default configurations file..." << std::endl;
        config.addr = DEFAULT_SERVER_ADDR;
        config.port = DEFAULT_SERVER_PORT;
        configurationFile.open("config", std::ios::out);
        configurationFile << "SERVER_ADDR=" << DEFAULT_SERVER_ADDR << std::endl;
        configurationFile << "SERVER_PORT=" << DEFAULT_SERVER_PORT << std::endl;
        configurationFile.close();
    }
    return config;
}
