#include "UsbDevice.h"
#include "Colors.h"
#include <fstream>
#include <string>
#include <sstream>
#include <string.h>
#include <unistd.h> // For sleep();
#include <mutex>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define DEVICE_CONFIRMED_CODE 100
#define DEVICE_REJECTED_CODE 101

static std::mutex bindFileMutex;
static std::mutex unbindFileMutex;

UsbDevice::UsbDevice(struct udev_device *device, server_config serverConfig)
{
    const char *dev_action = udev_device_get_action(device);
    const char *dev_vendor_id = udev_device_get_sysattr_value(device, "idVendor");
    const char *dev_product_id = udev_device_get_sysattr_value(device, "idProduct");
    const char *dev_subsystem = udev_device_get_subsystem(device);
    const char *dev_type = udev_device_get_devtype(device);
    const char *dev_node = udev_device_get_devnode(device);
    const char *dev_manufacture = udev_device_get_sysattr_value(device, "manufacturer");
    const char *dev_product_name = udev_device_get_sysattr_value(device, "product");
    const char *dev_serial_number = udev_device_get_sysattr_value(device, "serial");
    // Set default info to the device if there is no info about it
    if (!dev_action)
    {
        dev_action = "exists";
    }
    if (!dev_vendor_id)
    {
        dev_vendor_id = "0000";
    }
    if (!dev_product_id)
    {
        dev_product_id = "0000";
    }
    if (!dev_manufacture)
    {
        dev_manufacture = "Unknown manufacture";
    }
    if (!dev_product_name)
    {
        dev_product_name = "Unknown name";
    }
    if (!dev_serial_number)
    {
        dev_serial_number = "Unknown serial number";
    }
    this->_serverConfig = serverConfig;
    this->_device = device;
    this->_action = dev_action;
    this->_vendorId = dev_vendor_id;
    this->_productId = dev_product_id;
    this->_subsystem = dev_subsystem;
    this->_type = dev_type;
    this->_node = dev_node;
    this->_manufacture = dev_manufacture;
    this->_productName = dev_product_name;
    this->_serialNumber = dev_serial_number;
    // Grab the path to the device
	std::string path = udev_device_get_devpath(this->_device);
    // Gets the device's bus location from the path
    std::size_t last_slash_index = path.rfind("/");
    this->_busLocation = path.substr(last_slash_index + 1);
}

void UsbDevice::processDevice()
{
    std::cout << this << std::endl;
    int device_return_code = 0;
    if (this->_action == "add")
    {
        std::cout << "disconnecting..." << std::endl;
        this->disconnectDevice();
        device_return_code = sendDeviceRequest();
        if (device_return_code == DEVICE_CONFIRMED_CODE)
        {
            std::cout << "connecting..." << std::endl;
            this->connectDevice();
        }
    }
}

bool UsbDevice::disconnectDevice()
{
    std::ofstream unbind_file;
    unbindFileMutex.lock(); // Lock the file
    unbind_file.open("/sys/bus/usb/drivers/usb/unbind");
    if (!unbind_file.is_open())
    {
        unbind_file.close();
        return false;
    }

    //std::cout << "Location: " << this->_busLocation << std::endl; // Debug
    unbind_file << this->_busLocation;
    unbind_file.close();
    unbindFileMutex.unlock(); // Unlock the file
    return true;
}


bool UsbDevice::connectDevice()
{
    std::ofstream bind_file;
    bindFileMutex.lock();
    bind_file.open("/sys/bus/usb/drivers/usb/bind");
    if (!bind_file.is_open())
    {
        bind_file.close();
        return false;
    }

    //std::cout << "Location: " << this->_busLocation << std::endl; // Debug
    bind_file << this->_busLocation;
    bind_file.close();
    bindFileMutex.unlock();
    return true;
}

const std::string UsbDevice::usbDataToJson()
{
    std::ostringstream data;
    data << "{" << "\"VendorId\":\"" << this->_vendorId << "\"";
    data << ", \"ProductId\":\"" << this->_productId << "\"";
    data << ", \"SerialNumber\":\"" << this->_serialNumber <<"\"}";
    return data.str();
}

int UsbDevice::sendDeviceRequest()
{
    Color::Modifier red(Color::FG_RED);
    Color::Modifier green(Color::FG_GREEN);
    Color::Modifier def(Color::FG_DEFAULT);
    Color::Modifier yellow(Color::FG_YELLOW);

    std::string data;
    std::string retData;

    data = "CHECK|" + usbDataToJson();
    try
    {
        retData = sendMsgToServer(data);
    }
    catch (std::string &e)
    {
        std::cerr << e << std::endl;
        return DEVICE_REJECTED_CODE;
    }
    std::cout << "Device  " << this->_productId << ":" << this->_vendorId << "-" << this->_serialNumber <<
        " ";
    if (retData=="CONFIRMED") 
    {
        std::cout << green << retData << def << std::endl;
    }
    else if (retData=="REJECTED")
    {
        std::cout << red << retData << def << std::endl;
    }
    else
    {
        std::cout << yellow << retData << def << std::endl;
    }
    return retData == "CONFIRMED" ? DEVICE_CONFIRMED_CODE : DEVICE_REJECTED_CODE;

}

std::string UsbDevice::sendMsgToServer(std::string data)
{
    struct sockaddr_in address; 
    int sock = 0, valread; 
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};

    // std::cout << "Sending: " << data << std::endl; // Debug!!!

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) 
    { 
        throw std::string("Socket creation error"); 
    } 
   
    memset(&serv_addr, '0', sizeof(serv_addr)); 
   
    serv_addr.sin_family = AF_INET; 
    serv_addr.sin_port = htons(this->_serverConfig.port); 

    // Convert IPv4 and IPv6 addresses from text to binary form 
    if(inet_pton(AF_INET, this->_serverConfig.addr.c_str(), &serv_addr.sin_addr)<=0)  
    { 
        throw std::string("Invalid address/ Address not supported"); 
    } 
   
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
    { 
        throw std::string("Connection Failed"); 
    } 
    send(sock , data.c_str() , data.length() , 0 ); 
    
    valread = read( sock , buffer, 1024); 
    std::string retData = buffer;
    return retData;
}

std::ostream& operator<<(std::ostream& os, const UsbDevice& device)
{
    // os << "*********************************" << std::endl;
    // os << "sub-system: " << device._subsystem << std::endl <<
    //      "Type: " << device._type << std::endl << 
    //      "Action: " << device._action << std::endl <<
    //      "Manufacture: " << device._manufacture << std::endl <<
    //      "Name: " << device._productName << std::endl <<
    //      "ProductId:VendorId --> " << device._productId << ":" << device._vendorId << std::endl <<
    //      "Serial Number: " << device._serialNumber << std::endl;

    // return os;

    os <<
         "Action: " << device._action << std::endl <<
         "Name: " << device._productName << std::endl <<
         "VendorId:ProductId --> " << device._vendorId << ":" << device._productId << std::endl <<
         "Serial Number: " << device._serialNumber << std::endl;

    return os;
}

std::ostream& operator<<(std::ostream& os, const UsbDevice* device)
{
    os << *device;
    return os;
}

