#include <string>
#include <iostream>
#include <libudev.h>

typedef struct server_config{
    std::string addr;
    int port;
}server_config;

class UsbDevice
{
    public:
        UsbDevice(struct udev_device *device, server_config serverConfig);
        
        void processDevice();

        friend std::ostream& operator<<(std::ostream& os, const UsbDevice& device);
        friend std::ostream& operator<<(std::ostream& os, const UsbDevice* device);  


    private:
        server_config _serverConfig;
        struct udev_device *_device;
        std::string _vendorId;
        std::string _productId;
        std::string _serialNumber;
        std::string _action;
        std::string _subsystem;
        std::string _type;
        std::string _node;
        std::string _manufacture;
        std::string _productName;
        std::string _busLocation;

        bool disconnectDevice();
        bool connectDevice();
        int sendDeviceRequest();
        std::string sendMsgToServer(std::string data);
        const std::string usbDataToJson();
};

