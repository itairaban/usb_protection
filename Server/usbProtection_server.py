#!/usr/bin/env python3

import socket
import sys
import os
import json
import signal


HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 31337
BUFF_SIZE = 1024
WHITE_LIST = 1
BLACK_LIST = 2
DEVICE_CONFIRMED_CODE = "CONFIRMED"
DEVICE_REJECTED_CODE = "REJECTED"
ERROR_CODE = "ERROR"

# The communication protocol is:
# REQ_TYPE|{JSON_DATA}

class UsbDevice:
    
    def __init__(self, vid, pid, serial_number):
        self.vendorId = vid
        self.productId = pid
        self.serial_number = serial_number

    def __str__(self):
        return "{}:{}-{}".format(self.vendorId, self.productId, self.serial_number)

    def IsInList(self, list_type):
        file = None
        try:
            if list_type == WHITE_LIST:
                file = open("whitelist")
            elif list_type == BLACK_LIST:
                file = open("blacklist")
            else:
                return False
        except FileNotFoundError:  # If the file doesn't exists, return False
            return False
        
        fileData = file.readlines()
        file.close()
        return str(self) in fileData
            
    def AddToList(self, list_type):
        file = None
        if list_type == WHITE_LIST:
            file = open("whitelist", 'a+')
        elif list_type == BLACK_LIST:
            file = open("blacklist", 'a+')
        else:
            return None

        file.write(str(self))
        file.close()


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def signal_handler(signal, frame):
        print("\nBye!")
        sock.close()
        sys.exit()
    
    # signals handlers
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    try:
        sock.bind((HOST, PORT))
    except OSError:
        print(bcolors.FAIL + "[-] "  + bcolors.ENDC + "Can't bind to port " + str(PORT) + bcolors.ENDC, file=sys.stderr)
        sys.exit(1)

    sock.listen()

    while True:
        conn, addr = sock.accept()
        # print("New conn: ", addr)
        handleAgent(conn, addr)

    sock.close()


def handleAgent(conn, addr):
    data = getDataFromSocket(conn)
    #print(addr, " Data: ", data)  # Debug
    data = data.split('|', 1)
    #print(data)  # Debug
    if len(data) != 2 or not data[1].startswith('{') or not data[1].endswith('}'): # checks if the data fits the protocol
        print(bcolors.FAIL + "[-] "  + bcolors.ENDC + str(addr[0]) + ": " + "Invalid protocol" + bcolors.ENDC, file=sys.stderr)
        conn.sendall(ERROR_CODE.encode())
        return None
    req_type, data = data

    if req_type == "CHECK":
        dev_info = json.loads(data)  # convert the data from json to dict
        usb_device = UsbDevice(dev_info['VendorId'], dev_info['ProductId'], dev_info['SerialNumber'])  # Creates new usb device
        print(bcolors.OKBLUE + "[-] "  + bcolors.ENDC + "Request from agent " + addr[0] + ": ")
        
        if (usb_device.IsInList(WHITE_LIST)):
            conn.sendall(DEVICE_CONFIRMED_CODE.encode())
            print(bcolors.OKBLUE + "[-] " + bcolors.ENDC + "Device " + str(usb_device) + " " + bcolors.OKGREEN + DEVICE_CONFIRMED_CODE + bcolors.ENDC)
        elif (usb_device.IsInList(BLACK_LIST)):
            conn.sendall(DEVICE_REJECTED_CODE.encode())
            print(bcolors.OKBLUE + "[-] " + bcolors.ENDC + "Device " + str(usb_device) + " " + bcolors.FAIL + DEVICE_REJECTED_CODE + bcolors.ENDC)
        else:
            # Request for user's confirmation
            #print(bcolors.OKBLUE + "[-] "  + bcolors.ENDC + "Request from agent " + addr[0] + ": ")
            print("\tAccecpt Device (" + str(usb_device) + ")? " + bcolors.OKGREEN + "Y" + bcolors.ENDC + "/" + bcolors.FAIL + "N")
            
            while True:  # Gets user input and check that it is y or n
                ans = input(bcolors.WARNING + "\t--> " + bcolors.ENDC)
                if (ans == ""):
                    continue
                ans = ans[0].lower() # takes the first letter in case that the user typed yes or no
                if ans == 'y' or ans == 'n':
                    break
            
            if ans == 'y':
                usb_device.AddToList(WHITE_LIST)  # Adds the usb device to the whitelist
                conn.sendall(DEVICE_CONFIRMED_CODE.encode())
                print(bcolors.OKBLUE + "[-] " + bcolors.ENDC + "Device " + str(usb_device) + " " + bcolors.OKGREEN + DEVICE_CONFIRMED_CODE + bcolors.ENDC)
            else:
                usb_device.AddToList(BLACK_LIST)  # Adds the usb device to the blacklist
                conn.sendall(DEVICE_REJECTED_CODE.encode())
                print(bcolors.OKBLUE + "[-] " + bcolors.ENDC + "Device " + str(usb_device) + " " + bcolors.FAIL + DEVICE_REJECTED_CODE + bcolors.ENDC)
    else:
        conn.sendall(ERROR_CODE.encode())


def askUser(usb_device):
     pass


def getDataFromSocket(conn):
    # The function gets the data from the socket
    data = conn.recv(BUFF_SIZE)
    data = data.decode()
    return data.replace("\n", "") # removes the '\n'


if __name__ == '__main__':
    main()